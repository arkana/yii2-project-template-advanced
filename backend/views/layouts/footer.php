<footer class="main-footer">
    <strong>Copyright &copy; <?= date("Y") ?> <a href="https://arkanatechnology.com">Arkana Technology</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.2.0
    </div>
</footer>